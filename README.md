###   TitySQL是一个以数据为中心的DAL工具。

####  WIKI  [TitySQL使用手册](https://gitee.com/LeeFJ/tity-sql-public/wikis/pages)
    
####  设计理念 

&nbsp;&nbsp;&nbsp;TitySQL以 **SQL语句** 为中心( **DBA视角** )，结合DAO、SQL构建、记录集、非Pojo实体等工具进行快速数据库开发的DAL工具。


&nbsp;&nbsp;&nbsp;TitySQL构建的起点是 **企业应用** ，所以它是一个以 **数据为主** ，以 **实体为辅** 的一个DAO框架。企业用的特点就是 **多变** 。所以，TitySQL也是一个非常灵活的框架， **掌控感十足** 。

&nbsp;&nbsp;&nbsp;TitySQL在技术和代码层面 **简化开发过程** ，目的是使开发人员由技术聚焦转到 **业务聚焦** 和 **数据聚焦** 。客户围绕、业务围绕，以 **业务落地** 体现 **系统价值** 。

![占位符](https://images.gitee.com/uploads/images/2018/1230/092803_021390e6_1470521.png "empty.png")

#### 功能特点

&nbsp;&nbsp;&nbsp;TitySQL从 **DAO** 出发，扩展出数据库元数据、序列生成、增删改查、SQL构建、数据集、实体等方面的特性。

&nbsp;&nbsp;&nbsp;TitySQL支持Pojo实体，但是TitySQL生成的实体不是Pojo实体，而是一种 **类Pojo实体** 。TitySQL认为传统的Pojo实体 **清纯无邪** 非常好 ，但加入适当的功能，使她略显 **丰满但又不失干净** 。

&nbsp;&nbsp;&nbsp;TitySQL建议不缓存实体对象 ，而是缓存实体对象中的数据。TitySQL的RcdSet可以很 **轻松** 地把数据 **转换** 成JSON对象。
  
&nbsp;&nbsp;&nbsp;TitySQL支持SQL模板，TitySQL的SQL模板 **去除** 逻辑判断与逻辑控制。模板内提供 **占位符** ，用于替换那些可以变化的语句。

&nbsp;&nbsp;&nbsp;接下来，让我们一起感受一下TitySQL带来的开发之旅。

#### 配套开发框架


&nbsp;&nbsp;&nbsp;TitySQL可结合使用 [tity_platform](https://gitee.com/LeeFJ/tity_platform/wikis) 。

  